# Area Solo Check

### post-flight

# 20160616

* Southern Area Inbound checklist

  * ATIS.

  * Frequency changes.

  * Maintain 1500ft.

  * Compass/DG.

  * Park Ridge Water Tower.

    * Inbound radio call.

    * Heading bug 335.

    * Transponder 3000.

  * BUMFISH.

  * Radio call @ Logan Motorway.

  * ~20 degree right turn to join base for 28.

  * Aim for 1000ft at usual downwind->base point.

* Practice

  * Steep Turns

  * Forced Landings

  * Circuit (3 or 4 e.g. Glide)
  