# Steep Turns

# 20160519

### Objectives

* Recall the effect of LF on stall speed

* Recall distribution and magnitude of forces during a steep turn

* Recall the correct technique for entering/exiting steep turn

### Revision

Level Steep Turn

* any turn > 30 deg AoB

* maintain constant height while balanced

### Why Steep Turns?

* collision avoidance

* terrain avoidance

* getting through cloud gaps

* developing skills

### Forces

* more increase AoB to achieve level turn

* more IAS to achieve level turn

### Factors

* increase in stall speed, reduction in IAS

### Practical

* No steep turns < 95KIAS (see PoH)

* 1. Trimmed for Straight & Level

  2. Note reference height

  3. Note reference heading (heading bug, object in distance)

  4. Lookout

* Bank, Balance, Backpressure, Power

* 45 degrees initially, small amount of power (~100rpm)

* 60 degrees, full power

### Spiral Dive

* Symptoms

  * high and increasing IAS

  * high rate of descent

  * increase engine speed

  * increasing LF (airframe stress)

* Recovery

  * Idle Power

  * Level ailerons

  * Slowly elevator up to climb attitude

  * Allow airspeed to wash off

  * Apply power (caution: engine speed, air speed)

  * Recover lost height
