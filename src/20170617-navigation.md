# Navigation Ground School

## 20170617 Pathfinder Aviation, David Stehbens

#### Written material

##### Flight planning

###### Using the back of a Flight Notification Form

* AF - GYM - KRY - DBO - AF
* Numbers at the top of the columns represent the order in which each value is determined
  1. PSN Position. Note positions of flight plan.
  2. DIST. Using WAC and scale ruler, measure distance between positions (nm).
  3. TR(M) Track Magnetic
    * Using protractor, find Track True TR(T) between positions.
    * Using WAC, find and adjust for magnetic variation to determine Track Magnetic TR(M).
  4. WIND, FL or ALT
    * WIND. From NAIPS, obtain an area briefing for AREA 40 (south-east Queensland). Note the wind direction and velocity at different altitudes.
    * FL or ALT. Select an altitude that is consistent with VFR (if track west then even thousand + 500, else odd thousand + 500). Write `Axxx for altitude or `FLxxx` for flight level. Consider air space restrictions for selecting altitude or flight level.
  5. TAS True Airspeed.
    * Calculate TAS using flight computer. Using AIRSPEED CORRECTION, set pressure altitude against air temperature. Note also the Density Altitude. Read IAS off inner scale against TAS on outer scale.
    * Note TAS as `Nxxxx` in knots.
  6. G/S Ground Speed. Using wind side of flight computer.
    * Set WIND direction under TRUE INDEX.
    * Mark a point vertically up from the centre point to the amount of WIND velocity.
    * Set Track True TR(T) under the TRUE INDEX.
    * Slide WIND velocity mark to TAS.
    * Ground Speed is indicated under the centre point.
    * WIND correction angle is indicated as amount between centre line and WIND velocity mark.
  7. HDG(M) Heading Magnetic. Using the Track Magnetic TR(M) and the WIND correction angle calculated in previous step, add the correction angle to the TR(M) to obtain the HDG(M).
  8. ETI Estimated Time Interval. The ETI is `DIST * 60 / GS`. ETI can be calculated using the inner and outer scales on the flight computer to obtain the `1:1.6` ratio.
  9. Calculate
     * sum of leg distances to obtain total distance
     * sum of Estimated Time Intervals to obtain total time
     * average ground speed, `total distance * 60 / total time`

###### Flight planning additional notes to make on ground

* Select visual check points on each flight leg
* Draw a map of flight leg, including
  * altitudes
  * frequencies, including AWIS
  * fuel planning requirements
  * visual check points, including interval distances

###### The 1 in 60 rule

`sin x ~= x`

Example

On a 90nm leg, after a 30nm visual check point, find that your position is 5nm to the right of the check point. Correction angle for initial is `60/(30/5)=10` and with 60nm remaining, another correction `60/(60/5)=5` is required to track to point on flight leg.

Solution: TURN LEFT 15 DEGREES 

###### Additional notes

* `PA OAT      -> DA`
* `QNH ELE     -> PA`
* `QNH ELE OAT -> DA` *(therefore)*
* `IAS DA      -> TAS`
* `IAS PA OAT  -> TAS` *(therefore)*
* `IAS QNH ELE OAT -> TAS` *(therefore)* 

----

Simulated flight plan on WAC and Flight Notification Form

![Simulated flight plan on WAC and Flight Notification Form](http://i.imgur.com/Lkh6XpG.jpg)

----

Simulated flight plan on WAC and Flight Notification Form

![Simulated flight plan on WAC and Flight Notification Form](http://i.imgur.com/J7Vq6Rq.jpg)

----

Notes on filling out Flight Notification Form

![Notes on filling out Flight Notification Form](http://i.imgur.com/r2W9Z29.jpg)

----

Arbitrary notes

![Arbitrary notes](http://i.imgur.com/fzmTu1M.jpg)

----

Flight planning additional notes to make on ground

![Flight planning additional notes to make on ground](http://i.imgur.com/Iw91wdg.jpg)

----

Flight planning additional notes to make on ground

![Flight planning additional notes to make on ground](http://i.imgur.com/rEvRRet.jpg)

----

1 in 60 Rule

![1 in 60 Rule](http://i.imgur.com/G6bqr7Y.jpg)

----

1 in 60 Rule examples

![1 in 60 Rule examples](http://i.imgur.com/DrKGOfn.jpg)

----

Answers to specific questions

![Answers to specific questions](http://i.imgur.com/aixWV8c.jpg)

----

Calculating Density Altitude and True Airspeed on E6B

![Calculating Density Altitude and True Airspeed on E6B](http://i.imgur.com/KCVOOzB.jpg)

----

Flight plan on WAC

![Flight plan on WAC](http://i.imgur.com/hMJ4EYj.jpg)

----

Flight plan on WAC

![Flight plan on WAC](http://i.imgur.com/ASKNSAw.jpg)

----

Flight Notification Form

![Flight Notification Form](http://i.imgur.com/FoiKSJY.jpg)

----

Flight Notification Form

![Flight Notification Form](http://i.imgur.com/pB9VTYs.jpg)
