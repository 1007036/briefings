# PPL NAV 07 planning

### 20180214

### Next nav flight YBAF-SPMT-YWCK-YCFN-YTWB-GON-YBAF

* read YBAF ERSA on western depature

* read YAMB ERSA
  * VFR traffic
    * Spring Mountain
    * Lake Manchester
  * TRK to Spring Mountain (SPMT)
  * if YAMB clearance is unavailable, TRK to Flinders Peak, remaining under CTA (C LL 2500)

* as soon as out of YBAF zone contact YAMB DELIVERY
  * request code
  * if clearance is available, a code will be provided
    * contact YAMB APP

* plan for both:
  * Spring Mountain (YAMB clearance available)  
  * Flinders Peak (YAMB clearance unavailable)

* ring Clifton (Trevor -- Lone Eagle Flying School)

* Full Stop: Clifton
