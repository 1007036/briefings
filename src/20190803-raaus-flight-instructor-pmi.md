# RA Flight Instructor

## 20190803 Principles and Methods of Instruction (PMI)

## Flightscope Aviation, Rod Flockhart

* make patter notes (see phone images for examples)
* CAO 95.55 for aircraft limitations
* Laws of learning (7, see raaus FI manual page 4)
  * Readiness
  * Primacy
  * Relationship
  * Exercise
  * Intensity
  * Effect
  * Recency
* S&L
  * stability
    * e.g. pitch up 20 degrees, release stick, watch porpoising magnitude to slowly decrease back to stable
  * spiral stability
    * e.g. in an established spiral dive, releasing controls will not recover and stabilise the dive
* to practice:
  * off-airport forced landing
    * adopt Vg without using ASI
  * landing, holding off nose
  * stall recovery
    * maintain level flight
