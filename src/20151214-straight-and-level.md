# Straight and Level

### Aim

To understand the forces which affect the aircraft during straight and level flight, and to maintain constant height at varying airspeeds.

### Objectives

1. To recall from memory the power setting used for straight and level flight in a normal cruise configuration.

2. From memory, identify the four forces acting on an aeroplane in a straight and level flight.

3. Correctly recall from memory, the 2 work cycles to configure and maintain an aircraft for straight and level flight.

### Work Cycles

* Changing Cruise (PAST)

  * Power

  * Attitude

  * Speed

  * Trim

* Maintaining Cruise (ALAP)

  * Attitude

  * Lookout

  * Attitude

  * Performance

### Briefing Notes

* Four forces for straight and level flight

  1. Weight

    * **BEW** Basic Empty Weight

    * **MTOW** Maximum Take-Off Weight

    * **TOW** Take-Off Weight

  2. Lift

    * Increase AoA against decrease IAS and vice-versa

    * The lift equation, lift coefficient (Cl)

      * `L = Cl * (A * .5 * r * V^2)`

      * `Cl = L / (A * .5 * r * V^2)`

  3. Drag

    1. Induced Drag

      * By-product of lift

      * Decreases as IAS increases

    2.  Parasite Drag

      * Types

        * Form

        * Skin Friction

        * Interference

      * Increases as IAS increases, not as much as induced drag

    * Horizontal stabiliser accounts for higher induced drag against parasite drag as IAS increases

  4. Thrust

    * thrust = drag at straight and level

* Stability

  * Types

    * Positive

    * Neutral

    * Negative

  * Axes

    * Longitudinal axis

      * Axis on which aircraft rolls

      * Stability of pitch

      * Positive stability

    * Lateral axis

      * Axis on which aircraft pitches

      * Stability of roll

      * Neutral stability

    * Directional axis

      * Axis on which aircraft yaws

      * Positive stability

* Application

  * Normal cruise

    * Power = 2300 rpm 

    * "5 fingers" attitude

    * Gives performance = 100KIAS

  * Fast cruise

    * Power = 2500rpm

    * "6 fingers" attitude

    * Gives performance = 110KIAS

  * Slow cruise

    * Power = 2100rpm

    * "4 fingers" attitude

    * Gives performance = 80KIAS

  * Slow cruise (with flaps)

    * Power = 2100rpm

    * "5 fingers" attitude

    * Gives performance = 70KIAS

----

### Handout

![Handout page 1](images/20151214-straight-and-level-page1.jpg)

----

![Handout page 2](images/20151214-straight-and-level-page2.jpg)
