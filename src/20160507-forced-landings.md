# Forced Landings

# 20160507

### Considerations

* Glide range (lift:weight)

* Airspeed, **68KIAS best glide**

* Wind, moves the radius of available landing area

* Turning loses height

* C172, 1:9 glide at ~500ft/min

### Sources of determining wind direction

* low-level cloud

* water lanes on water

* dust and smoke

* ATIS

* drift of aeroplane

* windsock

### Field Selection

* **W**ind

* **O**bstacles

* **S**ize

* **S**hape

* **S**urface

* **S**lope

* **S**urrounding

* **S**un

* **C**ivilisation

### Application

1. Initial actions

  * conserve altitude, trim to 68KIAS

  * check

    * fuel selector on BOTH

    * fuel valve ON

    * mixture RICH

    * carburettor heat ON

    * throttle CYCLE

    * fuel pump ON

    * magnetos BOTH

2. Plan approach

  * select field

  * high key point, 2500ft

  * low key point, 1500ft

  * touch down point, 1/3 of the way into field

3. Trouble checks *(optional)*

  * fuel

  * carburettor heat

  * fuel mixture

  * pressures & temperatures

  * switches (magnetos)

  * throttle

4. Radio call *(see also ERSA on Emergency Procedures)*

  * MAYDAY (x3)

  * callsign (x3)

  * situation

  * position

    * distance

    * direction

    * altitude

  * intention

  * other useful information

  * set Transponder to `7700`

5. PAX briefing

  * situation

  * seat belts

  * brace position

6. Shut down checks

  * all systems off

    * fuel valve

    * mixture lean

    * throttle idle position

    * master switch OFF **only after flaps**

  * command PAX unlatch door on final

----

    +--4--HK-2--+    ^
    |           |    ^
    |           |    |
    5           |    |
    |           |   Wind
    |           |
    |           |
    LK    +     |
    |     |     |
    6     |     |
    +-----------+
