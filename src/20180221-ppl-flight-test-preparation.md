# PPL Flight Test Preparation

## 20180221

## Stephen Watson, Pathfinder Aviation

#### FLIGHT TEST REPORT Form 61-1488 Private Pilot Licence -- Aeroplane

*refer to [61.515 (2) (c) of CASR](http://www7.austlii.edu.au/cgi-bin/viewdb/au/legis/cth/consol_reg/casr1998333/s61.515.html)*

#### *GROUND COMPONENT excerpt of FLIGHT TEST*

###### GROUND COMPONENT 2.1 Knowledge Requirements

| Item No | Activities and Manoeuvres                                      | MOS Ref |
| ------- | -------------------------------------------------------------- | ------- |
| (a)     | Privileges and limitations of the licence with category rating | 2(a)    |
|         |                                                                |         |
| (b)     | Applicability of drug and alcohol regulations                  | 2(b)    |
|         |                                                                |         |
| (c)     | VFR aircraft instrument requirements                           | 2(c)    |
|         |                                                                |         |
| (d)     | Emergency equipment requirements                               | 2(d)    |
|         |                                                                |         |
| (e)     | Requirements for landing areas/aerodromes                      | 2(e)    |
|         |                                                                |         |
| (f)     | GNSS and its use in VFR navigation                             | 2(f)    |
|         |                                                                |         |
| (g)     | Fuel planning and oil requirements for the flight              | 2(g)    |
|         |                                                                |         |
| (h)     | Loading and unloading fuel                                     | 2(h)    |
|         |                                                                |         |
| (i)     | Managing cargo and passengers                                  | 2(i)    |
|         |                                                                |         |
| (j)     | Aircraft loading system                                        | 2(j)    |
|         |                                                                |         |
| (k)     | Aircraft performance and landing calculations                  | 2(k)    |
|         |                                                                |         |
| (l)     | Pilot maintenance authorisations                               | 2(l)    |
|         |                                                                |         |
| (m)     | Aircraft speed limitations                                     | 2(m)    |
|         |                                                                |         |
| (n)     | Aircraft systems                                               | 2(n)    |

----

###### abbreviations

* **CAR**: Civil Aviation Regulations 1988

* **CASR**: Civil Aviation Safety Regulations 1998

* **CAO**: Civil Aviation Order

* **CAAP**: Civil Aviation Advisory Publication

* **AIP**: Aeronautical Information Package

###### (a) Privileges and limitations of the licence with category rating

* the holder of a PPL is authorised to pilot an aircraft as pilot in command or co-pilot if: [CASR 61.505]
  * the aircraft is engaged in a private operation; or
  * the holder is receiving flight training.

* Civil Aviation Regulations 1988 2(7)(d) *definition of "private operation"*

  *see also VFRG 1.14*

```
(d)  an aircraft that is flying or operating for the purpose of, or in the course of:
  (i)    the personal transportation of the owner of the aircraft;
  (ii)   aerial spotting where no remuneration is received by the pilot or the owner of
         the aircraft or by any person or organisation on whose behalf the spotting is
         conducted;
  (iii)  agricultural operations on land owned and occupied by the owner of the aircraft;
  (iv)   aerial photography where no remuneration is received by the pilot or the owner of
         the aircraft or by any person or organisation on whose behalf the photography is
         conducted;
  (v)    the carriage of persons or the carriage of goods without a charge for the
         carriage being made other than the carriage, for the purposes of trade, of goods
         being the property of the pilot, the owner or the hirer of the aircraft;
  (va)   the carriage of persons in accordance with subregulation (7A);
  (vi)   the carriage of goods otherwise than for the purposes of trade;
  (vii)  flight training, other than the following:
    (A)    Part 141 flight training (within the meaning of regulation 141.015 of CASR);
    (B)    Part 142 flight training (within the meaning of regulation 142.015 of CASR);
    (C)    balloon flying training (within the meaning of subregulation 5.01(1)) for the
           grant of a balloon flight crew licence or rating; or
  (viii) any other activity of a kind substantially similar to any of those specified in
         subparagraphs (i) to (vi) (inclusive);
  shall be taken to be employed in private operations.
```

* On and after 1 September 2015, the holder of a PPL is authorised to exercise the privileges of the licence in a multi-crew operation only if the holder has completed an approved course of training in multi-crew cooperation. [CASR 61.510]

* The holder of a PPL that was granted on the basis of regulation 202.272 is taken to meet the requirement mentioned in subregulation (1) if, before 1 September 2015, the holder conducted a multi-crew operation. [CASR 61.510]

* The holder [of a pilot licence] is authorised to exercise the privileges of the licence in an aircraft of a particular category only if the holder also holds, as the associated aircraft category rating for the licence, the aircraft category rating for that category of aircraft. [CASR 61.375]

* The holder of a pilot licence is authorised to conduct a flight activity mentioned in column 2 of an item in table 61.1145 only if the holder also holds the endorsement mentioned in column 1 of the item. [CASR 61.380]

Table 61.1145 (abbreviated)

| Column 1 Endorsement                                      | Activities authorised                                                  | Requirements                                                                                           |
| --------------------------------------------------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| Aerobatics flight activity endorsement                    | Conduct aerobatic manoeuvres in an aeroplane above 3 000 ft AGL        | Aeroplane category rating, Spinning flight activity endorsement                                        |
|                                                           |                                                                        |                                                                                                        |
| Aerobatics (1 500) flight activity endorsement            | Conduct aerobatic manoeuvres in an aeroplane above 1 500 ft AGL        | Aeroplane category rating, Aerobatics flight activity endorsement                                      |
|                                                           |                                                                        |                                                                                                        |
| Aerobatics (1 000) flight activity endorsement            | Conduct aerobatic manoeuvres in an aeroplane above 1 000 ft AGL        | Aeroplane category rating, Aerobatics (1 500) flight activity endorsement                              |
|                                                           |                                                                        |                                                                                                        |
| Aerobatics (500) flight activity endorsement              | Conduct aerobatic manoeuvres in an aeroplane above 500 ft AGL          | Aeroplane category rating, Aerobatics (1 000) flight activity endorsement                              |
|                                                           |                                                                        |                                                                                                        |
| Aerobatics (unlimited) flight activity endorsement        | Conduct aerobatic manoeuvres in an aeroplane at any height             | Aeroplane category rating, Aerobatics (500) flight activity endorsement                                |
|                                                           |                                                                        |                                                                                                        |
| Formation flying (aeroplane) flight activity endorsement  | Conduct formation flying in an aeroplane                               | Aeroplane category rating                                                                              |
|                                                           |                                                                        |                                                                                                        |
| Formation aerobatics flight activity endorsement          | Conduct aerobatic manoeuvres in an aeroplane while flying in formation | Aeroplane category rating, Aerobatics flight activity endorsement, Formation flying (aeroplane) flight activity endorsement | 
|                                                           |                                                                        |                                                                                                        |
| Spinning flight activity endorsement                      | Conduct intentional upright spinning manoeuvres above 3 000 ft AGL     | Aeroplane category rating                                                                              |
|                                                           |                                                                        |                                                                                                        |
| Formation flying (helicopter) flight activity endorsement | Conduct formation flying in a helicopter                               | Helicopter category rating                                                                             |

* The holder of a pilot licence is authorised to exercise the privileges of the licence in an aircraft that has a design feature mentioned in regulation 61.755 for the aircraft only if the holder also holds the design feature endorsement for the design feature. [CASR 61.380]
* Design features that require design feature endorsement [CASR 61.755 (Aeroplanes)]
  * tailwheel undercarriage
  * retractable undercarriage
  * manual propellor pitch control (piston engine)
  * gas turbine engine
  * multi-engine centre-line thrust
  * pressurisation system
  * floatplane
  * floating hull
  * ski landing gear

* The holder of a pilot licence is authorised to exercise the privileges of the licence in an aircraft only if the holder is competent in operating the aircraft to the standards mentioned in the Part 61 Manual of Standards for the class or type to which the aircraft belongs, including in all of the following areas: [CASR 61.385 (1)]
  * operating the aircraft's navigation and operating systems;
  * conducting all normal, abnormal and emergency flight procedures for the aircraft;
  * applying operating limitations;
  * weight and balance requirements;
  * applying aircraft performance data, including take-off and landing performance data, for the aircraft. 

* The holder of a pilot licence is authorised to exercise the privileges of the licence in an aircraft that has an operative airborne collision avoidance system only if the holder is competent in the use of an airborne collision avoidance system to the standards mentioned in the Part 61 Manual of Standards. [CASR 61.385 (2)]

* The holder of a pilot licence is authorised to pilot, during take-off or landing, an aircraft of a particular category carrying a passenger by day only if the holder has, within the previous 90 days, in an aircraft of that category or an approved flight simulator for the purpose, conducted, by day or night: [CASR 61.395 (1)]
  * at least 3 take-offs
  * at least 3 landings
  * while controlling the aircraft or flight simulator. 
  * the following are taken to have met the requirements for 3 take-offs and 3 landings
    * flight check
    * flight review
    * operator training and checking systemand operator approved for operations in that aircraft category

* The holder of a PPL is authorised to exercise the privileges of the licence only if the holder also holds one of: [CASR 61.410 (1)]
  * a current class 1 or 2 medical certificate
  * a medical exemption for the exercise of the privileges of the licence. 
  * RAMPC in a recreational aircraft, day VFR, meeting the requirements for RPL (CASR 61.405 (2))

* The holder of a CPL requires a class 1 medical, but may exercise the privileges of a PPL with a class 2 medical, or RPL with a RAMPC [CASR 61.415]

* The holder of a pilot licence is authorised to exercise the privileges of the licence on a flight only if the holder carries the following documents on the flight: [CASR 61.420]
  * licence document
  * medical certificate
  * a document that includes a photograph of the holder showing the holder's full face and his or her head and shoulders

* The holder of a pilot licence other than a recreational pilot licence is authorised to exercise the privileges of the licence only if the holder has a current aviation English language proficiency assessment. [CASR 61.422]

* The holder of a pilot licence is authorised to pilot an aircraft only if the aircraft is registered. [CASR 61.425]

* The 25nm limit is removed if [CASR 61.427]
  * an application is made to CASA
  * any of the following are granted
    * controlled airspace endorsement
    * PPL
    * CPL

----

###### (b) Applicability of drug and alcohol regulations

*see also CIVIL AVIATION SAFETY REGULATIONS 1998 PART 99--DRUG AND ALCOHOL MANAGEMENT PLANS AND TESTING*

* A person shall not, while in a state of intoxication, enter any aircraft. [CAR 256 (1)]

* A person acting as a member of the operating crew of an aircraft, or carried in the aircraft to act as a member of the operating crew, shall not, while so acting or carried, be in a state in which, by reason of his or her having consumed, used, or absorbed any alcoholic liquor, drug, pharmaceutical or medicinal preparation or other substance, his or her capacity so to act is impaired. [CAR 256 (2)]

* A person shall not act as, or perform any duties or functions preparatory to acting as, a member of the operating crew of an aircraft if the person has, during the period of 8 hours immediately preceding the departure of the aircraft consumed any alcoholic liquor. [CAR 256 (3)]

* A person who is on board an aircraft as a member of the operating crew, or as a person carried in the aircraft for the purpose of acting as a member of the operating crew, shall not consume any alcoholic liquor. [CAR 256 (4)]

----

###### (c) VFR aircraft instrument requirements

*see also [Civil Aviation Regulations 1988 - REG 174A](http://www5.austlii.edu.au/au/legis/cth/consol_reg/car1988263/s174a.html)*

* Instruments required for flight under the V.F.R. [CAO 20.18 (Appendix I)]
  * The flight and navigational instruments required for flights under the V.F.R. are:
    * airspeed indicating system
    * an altimeter, with a readily adjustable pressure datum setting scale graduated in millibars
    * a direct reading magnetic compass; or a remote indicating compass and a standby direct reading magnetic compass
    * an accurate timepiece indicating the time in hours, minutes and seconds.
  * In addition to the instruments required under the V.F.R., aircraft, other than helicopters, engaged in charter, or aerial work, operations and operating under the V.F.R., must be equipped with:
    * a turn and slip indicator (agricultural aeroplanes may be equipped with a slip indicator only)
    * an outside air temperature indicator when operating from an aerodrome at which ambient air temperature is not available from ground-based instruments.

* Pre-flight Altimeter Check
  * Whenever an accurate QNH is available and the aircraft is at a known elevation, pilots must conduct an accuracy check of the aircraft altimeter(s) at some point prior to takeoff.

    Note: Where the first check indicates that an altimeter is unserviceable, the pilot is permitted to conduct a further check at another location on the same airfield; for example, the first on the tarmac and the second at the runway threshold (to determine altimeter serviceability). [AIP ENR 1.7 (1.3.1)]
  * With an accurate QNH set, a VFR altimeter(s) should read site elevation to within 100FT (110FT at test sites above 3,300FT) to be accepted as serviceable by the pilot. If an aircraft fitted with two VFR altimeters continues to fly with one altimeter reading 100FT (110FT) or more in error, the faulty altimeter must be placarded unserviceable and the error noted in the maintenance release. [AIP ENR 1.7 (1.3.1)]

  * VFR altimeters are not permitted for aeroplane operations above FL200. VFR flights operating above FL200 must be equipped with an altimeter calibrated to IFR standards. [AIP ENR 1.7 (1.3.2)]

  * A QNH can be considered accurate if it is provided by ATIS, Tower or an automatic remote-reporting aerodrome sensor. Area or forecast QNH must not be used for the test. [AIP ENR 1.7 (1.4.1)]

* Site elevation must be derived from aerodrome survey data published by Airservices or supplied by the aerodrome owner. [AIP ENR 1.7 (1.4.2)]
* For all operations at or below the Transition Altitude, the altimeter reference setting will be: [AIP ENR 1.7 (2.1.2)]
  * the current Local QNH of a station along the route within 100NM of the aircraft; or
  * the current Area QNH forecast if the current Local QNH is not known.
* QNH is available from a reporting station, the ATIS, the Terminal Area Forecast, Area QNH forecast, AERIS, or from ATS [AIP ENR 1.7 (2.1.5)]

* Area QNH is a forecast value which is valid for a period of 3 hours and normally applies throughout an Area QNH Zone (AQZ). [AIP ENR 1.7 (2.2.1)]

----

###### (d) Emergency equipment requirements

* life jackets when over water and out of glide [CAO 20.11 (5.1)]
* sufficient life raft(s) when minimum(30 minutes cruise, 100nm) [CAO 20.11 (5.2.1)]
* if life raft required, 1 ELT and pyro distress signals [CAO 20.11 (6.1)]
* if more than one life raft required, then >= 2 (approved ELT under reg 252A) transmitters 121.5MHz and 243MHz and stowed ready for use [CAO 20.11 (6.1)]
* single-engine, over water, that is not equipped with radio or incapable of air-to-ground radio, not required to carry a life raft, shall carry ELT (121.5MHz and 243MHz approved under reg 252A) [CAO 20.11 (6.2)]

----

###### (e) Requirements for landing areas/aerodromes

* Recommended minimum physical characteristics of landing areas [CAAP 92-1 (5)]
  * Single engined and Centre-Line Thrust Aeroplanes not exceeding 2000kg MTOW (day operations) [CAAP 92-1 FIGURE 2A]
    ![CAAP 92-1 FIGURE 2A](images/CAAP_92-1_FIGURE2A.png)

* In take-off configuration, landing gear extended, engines at take-off power, an aeroplane must achieve a climb gradient of 6%. [CAO 20.7.4 (7)]

* In landing configuration, engines at take-off power, ISA, an aeroplane must achieve a climb gradient of 3.2% at 1.3VS. [CAO 20.7.4 (9)]

* [ERSA INTRODUCTION 1.7 Climb/Descent Gradient Graph]

  ![ERSA INTRODUCTION 1.7 Climb/Descent Gradient Graph](images/ERSA_INTRODUCTION_1.7_CLIMBDESCENTGRAPH.png)

----

###### (f) GNSS and its use in VFR navigation

GNSS can be used as a supplement to navigation, not primary. No training is required to use GNSS. [AIP GEN 1.5]

----

###### (g) Fuel planning and oil requirements for the flight

* **fixed fuel reserve** means an amount of fuel, expressed as a period of time holding at
1500 feet above an aerodrome at standard atmospheric conditions, that may be used for
unplanned manoeuvring in the vicinity of the aerodrome at which it is proposed to land,
and that would normally be retained in the aircraft until the final landing. [CAAP 234-1]

* **holding fuel** means an amount of fuel that will allow an aircraft to fly for a specified
period of time, being an amount  that is calculated at the holding rate established for
the aircraft at a level not greater than flight level 200 and at a temperature not less
than forecast. [CAAP 234-1]

* **variable fuel reserve** means an amount of fuel on board an aircraft that is sufficient to
provide for unexpected fuel consumption caused by factors other than a loss of
pressurisation or an engine failure. [CAAP 234-1]

* Variable Fuel Reserve, piston aeroplane [CAAP 234-1]
  * private and aerial work: `nil`
  * transport and charter: `15%`
  * *Note: For flights with sectors of 3 hours or more between suitable aerodromes at
    which a safe landing  can be made, it is recommended that private and aerial work
    flights carry a variable reserve of 10%.*
* Fixed Fuel Reserve, piston aeroplane: `45 minutes`

* Holding must be in accordance with approved procedures. If aircraft are required to hold at a point for which no procedure is published, they must do so in a manner specified by ATC. [AIP ENR 1.1 (11.4.1)]

* A request by a pilot in command to deviate from a prescribed holding procedure may be
approved. [AIP ENR 1.1 (11.4.2)]

* The pilot in command is responsible for taking appropriate diversion action based on information received. The pilot must provide the latest diversion time from the destination or from a point en route and, if required, the time interval. [AIP ENR 1.1 (11.5.1)]

* Except when operating an aircraft under the VFR by day within 50NM of the point of departure, the pilot in command must provide for a suitable alternate aerodrome when arrival at the destination will be during the currency of, or up to 30 minutes prior to the forecast commencement of, the following weather conditions: [AIP ENR 1.1 (11.8.2.1)]
  * cloud - more than SCT below the alternate minimum (see paras 11.8.2.12 and 11.8.2.13); or
    * Note: In determining requirements for alternate aerodromes, forecast amounts of cloud below the alternate minima are cumulative. For determining requirements, the cumulative cloud amount is interpreted as follows:   
      * FEW plus FEW is equivalent to SCT,
      * FEW plus SCT is equivalent to BKN,
      * SCT plus SCT is equivalent to BKN or OVC.
  * visibility - less than the alternate minimum; or
  * visibility - greater than the alternate minimum, but the forecast is endorsed with a percentage probability of fog, mist, dust or any other phenomenon restricting visibility below the alternate minimum; or
  * wind - a crosswind or tailwind component more than the maximum for the aircraft.
  * Note: Wind gusts must be considered.

* When weather conditions at the destination are forecast to be above the values specified at para 11.8.2.1, but, additionally, intermittent or temporary deteriorations in the weather below the values are forecast, provision of an alternate need not be made if sufficient additional fuel is carried to allow the aircraft to hold for: [AIP ENR 1.1 (11.8.2.4)]
  * 30 minutes for intermittent deterioration (INTER); and
  * 60 minutes for temporary deterioration (TEMPO).

* The alternate minimum for flight by aeroplanes under the VFR (day or night) and helicopters operating under the VFR at night, the alternate minima are a ceiling of 1,500FT and a visibility of 8KM. [AIP ENR 1.1 (11.8.2.13)]

----

###### (h) Loading and unloading fuel

*see also VFRG 1.18*

* During fuelling operations, the aircraft and ground fuelling equipment shall be so located that no fuel tank filling points or vent outlets lie: [CAO 20.9 (4.1.1)]
  * within 5 metres (17 ft) of any sealed building; and
  * within 6 metres (20 ft) of other stationary aircraft
  * within 15 metres (50 ft) of any exposed public area
  * within 15 metres (50 ft) of any unsealed building in the case of aircraft with a maximum take-off weight in excess of 5 700 kg (12 566 lb)
  * within 9 metres (30 ft) of any unsealed building in the case of aircraft with a maximum take-off weight not exceeding 5 700 kg (12 566 lb)

* Civil Aviation Order 20.9, Fuelling of Aircraft, Location of Aircraft (4.1.1.2)

* a sealed building is one which all the external part within 15 metres (50 ft) of an aircraft's fuel tank filling points or vent outlets or ground  fuelling equipment is of non-flammable materials and has no openings or all openings are closed. [CAO 20.9 (4.1.1.2)

* The operator of an aircraft must ensure that avgas is not loaded onto an aircraft while passengers are on board, or entering or leaving, the aircraft. [CAO (4.2.1)]

* When an external electrical supply is used, the connections between that supply and the aircraft electrical system shall be made and securely locked before a fuelling operation. [CAO 20.9 (4.3.2)]

----

###### (i) Managing cargo and passengers

* one member of flight crew must be wearing seat belt at all times [CAO 20.16.3]
* all persons wearing seat belt: [CAO 20.16.3]
  * during take-off and landing
  * during an instrument approach
  * when <= 1000ft AGL (unless CASA directs)
  * in turbulent conditions

* Passengers briefed before take-off [CAO 20.11 (14)]
  * smoking requirements
  * use and adjustment of seat belts
  * location and use of emergency exits
  * use of oxygen where applicable
  * use of flotation devices where applicable
  * baggage area
  * survival equipment where applicable

* Passengers in seat with dual controls [CAO 20.16.3 (11), CAR 228] 
  * instructed to not interfere with controls
  * satisfactory communication between PiC and passenger(s)
  * *see also CAAP 253-2 (20)*

----

###### (j) Aircraft loading system

*N/A*

----

###### (k) Aircraft performance and landing calculations

*N/A*

----

###### (l) Pilot maintenance authorisations

* It is an offence to authorise maintenance on an aircraft in Australian territory by a person who is not permitted to carry out the maintenance [CAR 42ZC (1)]

* This CAAP provides a copy of the instructions contained in Schedule 8 of CAR 1988 which specifies the maintenance that may be performed by pilots of class B aircraft (other than a student pilot). [CAAP 42ZC-1 (2.1)]

* Maintenance that may be carried out on a Class B aircraft by a person entitled to do so under subregulation 42ZC(4)

  * Removal or installation of landing gear tyres, but only if the removal or installation does not involve the complete jacking of the aircraft. 

  * Repair of pneumatic tubes of landing gear tyres. 

  * Servicing of landing gear wheel bearings.
 
  * Replacement of defective safety wiring or split pins, but not including wiring or pins in control systems.

  * Removal or refitting of a door, but only if:

    * no disassembly of the primary structure or operating system of the aircraft is involved; and

    * if the aircraft is to be operated with the door removed--the aircraft has a flight manual and the manual indicates that the aircraft may be operated with the door removed.

  * Replacement of side windows in an unpressurised aircraft.

  * Replacement of seats, but only if the replacement does not involve disassembly of any part of the primary structure of the aircraft.

  * Repairs to the upholstery or decorative furnishings of the interior of the cabin or cockpit.

  * Replacement of seat belts or harnesses.

  * Replacement or repair of signs and markings.

  * Replacement of bulbs, reflectors, glasses, lenses or lights.

  * Replacement, cleaning, or setting gaps of, spark plugs.

  * Replacement of batteries.

  * Changing oil filters or air filters.

  * Changing or replenishing engine oil or fuel.

  * Lubrication not requiring disassembly or requiring only the removal of non-structural parts, or of cover plates, cowlings and fairings.

  * Replenishment of hydraulic fluid.

  * Application of preservative or protective materials, but only if no disassembly of the primary structure or operating system of the aircraft is involved.

  * Removal or replacement of equipment used for agricultural purposes.

  * Removal or replacement of glider tow hooks.

  * Carrying out of an inspection under regulation 42G of a flight control system that has been assembled, adjusted, repaired, modified or replaced.

  * Carrying out of a daily inspection of an aircraft.

  * Connection and disconnection of optional dual control in an aircraft without the use of any tools for the purpose of transitioning the aircraft from single to dual, or dual to single, pilot operation.

  * Inspections or checks set out in the following documents in circumstances where the document clearly states that the maintenance may be carried out by the pilot of the aircraft and the maintenance does not require the use of any tools or equipment:

    * the aircraft's approved maintenance data;

    * the aircraft's flight manual or an equivalent document;

    * any instructions issued by the NAA that approved the type certificate for the aircraft.

  * For an aircraft that is installed with an oxygen system for the exclusive use of ill or injured persons on an aircraft used to perform ambulance functions--replenishing the oxygen system installed on the aircraft. 

----

###### (m) Aircraft speed limitations

----

###### (n) Aircraft systems
