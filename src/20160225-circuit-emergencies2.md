# Circuit Emergencies

# 20160225

### Glide Approach

* Ignore "turn base at 45 degrees." Begin late downwind.

* Simulated engine failure, hold nose attitude up.

* Trim for 70KIAS.

* Adopt attitude, similar to Straight & Level.

* Immediately turn base, 30 degrees AoB.

* Immediately wings level after turn and in balance.

* Reassess

  * touch-down aimpoint rising -> falling short, therefore begin turn to
    threshold.

  * touch-down aimpoint falling -> overshooting, therefore either:

    * continue track, take flap.

    * turn away from runway before turn to final.

##### Workflow

* **A**viate

* **N**avigate

* **C**ommunicate

* **A**dministrate
