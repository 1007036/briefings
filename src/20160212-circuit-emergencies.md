# Circuit Emergencies

# 20160212

### Go-around

* No fixed parameters to determine go-around

* Set personal limits

* Decide, am I going around?

  * Full power

  * Raise nose

  * Cowl on horizon

  * Flap up one stage, 30 to 20

  * Achieve minimum 56KIAS

  * At ~60KIAS, flap up one stage, 20 to 10

  * At ~65KIAS, flap retracted, 10 to 0

  * Raise nose to climb

### Flapless Approach

* Trim nose up

* Higher attitudes than normal

* Trim substitutes flap

* 75-80KIAS on base

* Power for speed

* 70KIAS approach

* Threshold on top of cowling

### EFATO

* During take-off

* After take-off with insufficient runway

* After take-off with sufficient runway

### Glide Approach

*todo*