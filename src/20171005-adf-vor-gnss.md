# 20171005 ADF/VOR/GNSS

* ADF
  * Two antennae
    * Loop, small box underneath aircraft (determines which of two directions signal is received)
    * Sense, Wire along length of aircraft
  * To tune to broadcast station
    * Tune
    * Press ADF on COM
    * Select ADF on ADF (to IDENT)
  * To tune to NDB
    * Tune frequency
    * Select ANT on ADF
    * Select ADF on COM
    * Test by Turning ADF knob to TEST; observe ADF needle turning
  * NDB loses range by hours of night (HN)
* VOR
  * Range does not depend on power; depends on Altitude (line of sight)
  * 5000ft AGL ~ 60nm range
  * 10000ft AGL ~ 90nm range
* GNSS
  * Garmin G430 (146)
    * See Quick Reference Handbook
    * Make flight plan on FlightPlan.xsd
    * Simulator needs Windows XP
  * Diversion on flight test
    * e.g. divert to YBAF
    * draw line on WAC
    * approximate HDG
    * approximate DIST
    * select DIRECT TO on G430, enter YBAF
  * See CAAP on GNSS
  