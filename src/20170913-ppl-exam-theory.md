# PPL exam theory

## 20170913 Alyce

* Session 1
  * BAK Knowledge Review
  * Aeroplane AGK  Aerodynamics
  * Air Law
* Session 2
  * PPL Navigation
  * GNSS
  * Human Factors
* Session 3
  * Meteorology
  * Performance
  * Summary & Revision

##### Ammeter

* **Load type** alternator output only (never goes below zero)
* **Centre zero type** Indicates total charge, including battery & alternator

----

#### 20171004 Session 3

###### layers of the atmosphere

* troposphere
* tropopause
  * starts at 36080ft (ISA)
  * -56C (ISA)
  * ~8km at poles
  * ~18km at equator
* stratosphere
  * 66000ft

###### precipitation

* shower
* rain
* virga

###### cloud types (todo: get pic on phone)

* cloud base (x1000ft) = (temp - dewpoint)/2.5C

* convective ascent

* orographic ascent

* widespread ascent

* turbulent mixing

* conductive cooling

* clouds, factors & considerations (todo: get pic on phone)

* wind shear

###### forecasts

* TAF
  * 12 hour validity
  * 3nm from aerodrome

* ARFOR
  * CB or TCU clouds
  * cloud base < 10000ft AMSL
  * cloud associated with precipitation

* METAR

###### Atmospheric Stability

* Adiabatic Process
  * Dry Lapse Rate 3C/1000ft (ISA)
  * Saturated Lapse Rate 1.5C/1000ft (ISA)
* Environmental Lapse Rate
  * actual change not theoretical or ISA
  * inversion layers affect ELR
* (todo: get pic on phone)

* Fog
  * Mist: visibility >= 1000 metres, < 95% humidity
  * Fog: visibility < 1000 metres, ~ 100% humidity
  * Radiation Fog
    * moist air cools below dew point by contact with cold surface that is losing its heat by radiation
    * high humidity
    * cloudless
    * light winds
    * anticyclones
  * Advection Fog
    * warm moist air moves over cooler surface. Requires some turbulence (6-15kt)
  * Sea Fog
    * type of advection fog
    * persistent
  * Steam Fog
    * evaporates from water into overlying cool air
  * Frontal Fog
    * boundary of two air masses within a single air mass
  * Upslope Fog
    * moist air forced up a terrain slop and cools to saturation

###### Maximum endurance and range

* endurance: time
* range: distance

* Maximum endurance
  * ~75% of maximum range
  * Equal to Vy

* Maximum range
  * TAS for minimum total drag
  * Best ratio of speed to fuel
  * Ground range
  * Air range

* Maximum (specific air) range
  * ~ 20% < cruise speed

* CAO 20.7.0
* CAO 20.7.4

* Declared Density Height
