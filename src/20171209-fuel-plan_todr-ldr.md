# Briefing

### Marcelo Genta, 20171209

##### Fuel planning

* Calculate minutes required for one leg on navigation flight.
* Example
  * 70 minutes YBAF to YWND with `INTER`, no `TEMPO`, no alternate.
  * 66 minutes YWND to YBAF with no `INTER`, no `TEMPO`, no alternate.
* According to PathFinder ops manual, fuel burn rates for KJR: `35L/hr` cruise, `30L/hr` holding.
* On fuel calculation plan, enter `Cruise` and `Min` as `70`. Convert to `L` by cruise burn rate (`35L/hr -> 41L`) and enter value (`41`).
* If alternate is required, calculate distance on charts, enter flight time required to divert to alternate, convert to `L` at cruise burn rate and enter value. Since no alternate in this example: `0`.
* Sum the climb, cruise and alternate `Min` and `L or kg` to the intermediate `Sub-total`.
* See CAAP 234-1 13. Fuel quantity cross-check for fuel required for Variable Reserve and Fixed Reserve. In this case:
  * **Table 1 Variable fuel reserve** Aeroplane and airships, Private and aerial work: `Nil`. Enter `0` for `Variable Reserve`.
  * **Table 2 Fixed fuel reserve** Aeroplanes and airships, VFR and IFR, Piston engined: `45 minutes`. Enter `45` for `Fixed Reserve` and convert to `L` using cruise fuel burn rate (`26`).
* AIP ENR 1.1 - 95 (11.8.2.1)

    Except when operating an aircraft under the VFR by day within 50NM  of  the  point  of  departure, the pilot in command must provide for a suitable alternate aerodrome when arrival at the destination will be during the currency of, or up to 30 minutes prior to the forecast  commencement of, the following weather conditions:
    * cloud - more than SCT below the alternate minimum (see paras 11.8.2.12 and 11.8.2.13); or
    Note: In determining requirements for alternate aerodromes, forecast amounts of cloud below  the alternate minima are cumulative. For determining requirements, the cumulative cloud amount is interpreted as follows:     
      * FEW plus FEW is equivalent to SCT,
      * FEW plus SCT is equivalent to BKN,
      * SCT plus SCT is equivalent to BKN or OVC.
    * visibility - less than the alternate minimum; or
    * visibility - greater than the alternate minimum, but the forecast is endorsed with a percentage probability of fog, mist, dust or any other phenomenon restricting visibility below   the alternate minimum; or
    * wind - a crosswind or tailwind component more than the maximum for the aircraft.
    *Note: Wind gusts must be considered.*
* AIP ENR 1.1 - 95 (11.8.2.4)
  When weather conditions at the destination are forecast to be above the values specified at para  11.8.2.1, but, additionally, intermittent or temporary deteriorations in the weather below the values are forecast, provision of an alternate need not be made if sufficient additional fuel is carried to allow the aircraft to hold for:
    * 30 minutes for intermittent deterioration (INTER); and
    * 60 minutes for temporary deterioration (TEMPO).
* Since `INTER` assumed for the example, enter `30` into `Min` for `Holding`. Convert to `L` by holding burn rate (`30L/hr -> 15L`) and enter value (`15`).
* According to PathFinder ops manual, `5L` for ground operations. Enter `5` into `L or kg` for `Taxi`. Convert to `Min` by cruise burn rate (35L/hr -> `8.6 minutes`). Enter `9` into `Min` for `Taxi`.
* Sum `Sub-Total`, `Variable Reserve`, `Fixed Reserve`, `Holding`, `Taxi` to achieve `Fuel Require`.
    * `Min`: `70+0+45+30+9=154`.
    * `L`: `41+0+26+15+5=87`.
* Next leg: YWND to YBAF. from previous leg transfer sum `Min` of `Fuel Margin`, `Fixed Reserve` to `Min` of `Fuel Endurance` on next leg.
* On fuel calculation plan, enter `Cruise` and `Min` as `66`. Convert to `L` by cruise burn rate (`35L/hr -> 39L`) and enter value (`39`).
* Repeat all steps for previous leg to achieve `Fuel Require` and `Fuel Margin`.
* Calculate the fuel endurance expected upon completion of the last leg at YBAF.
  * Sum the `L` for `Fuel Margin` and `Fixed Reserve` (`66+26=92`).

![Example fuel plan](https://gitlab.com/1007036/notes/raw/master/src/fuel-plan-example.jpg)

----

##### TODR/LDR

Use calculated fuel endurance at landing and take-off, determine weight for each touch/go or full-stop.

* Example:
  * YBAF to YWND (full-stop)
  * YWND to YBAF (full-stop)

* Since fuel-burn quantity is so small on each leg, round to 2000lb for each TODR calculation.
* Determine the Pressure Altitude (PA) and temperature for the airfield.
  * Use METAR if available.
  * Since YWND does not have a METAR, use the declared density chart in CAO 20.7.0 (also VFRG).
    * For December at YWND, +2800ft, with a field elevation of 1050ft, therefore `DA=3850ft`.
  * ISA temperature at YWND = `13C` with a lapse rate of `2C/1000ft`, deviating from sea-level ISA temperature (`15C`).
  * Linearly interpolate on take-off distance chart at `2000lb`, `temp=13C`, `PA=3850` to achieve approximately:
    * `822ft` ground roll
    * `1512ft` to clear 50ft obstacle
  * Since YWND is unpaved, follow PoH by adding 15% to ground roll.
    * calculate non-ground roll `1512ft-822ft=690ft`.
    * add 15% to ground roll `822ft*1.15=945ft`.
    * total take-off `690ft+945ft=1635ft`.
  * Add 15% according to CAO 20.7.4 to achieve total TODR.
    * `1635ft*1.15=1880ft`.
    * `1880ft=573m`.
  * Observe YWND runway distance at `1404m`.
* Repeat for LDR at MTOW.
* Repeat for each leg that has a landing/take-off in the flight plan (i.e. YBAF).
