# Circuits

# 20160108

### Circuit Legs

1. Upwind

2. Crosswind

  * The cue to turn onto crosswind from upwind is 500ft AGL.

  * Level off at 1000ft AGL.

3. Downwind

  * The cue to turn onto downwind from crosswind is 0.75 to 1.0NM from runway.

  * In C172S out window, runway is 2/3 up the strut.

4. Base

  * The cue to turn base is 45 degrees from runway threshold.

  * Reduce power.

  * Begin descent.

5. Final

  * Turn at ~600ft AGL.

  * After turn, wings level at 500ft AGL.

  * Don't overshoot turn because parallel runway at YBAF.

  * Use power for airspeed.

  * Attitude for aim point on runway.

----

### Checklist at Holding Point

* Strobe & Landing ON

* Pumps (as required)

* Transponder

  * Standby

  * Altitude

* Heat

  * Pitot Heat (as required)

  * Cabin Heater

### Circuits in C172S

##### Take-off

* Full-power

  * Section 4 in manual

  * Static power 2300rpm minimum

  * Check Pressures and Temperatures

  * IAS increasing

* 55KIAS rotate

* Attitude ~ cowl on horizon

* Vx ~ 62KIAS

* Vy ~ 74KIAS

* At Vy, raise nose slightly higher

* 300ft take-off checks

  * Full power

  * >= 2300rpm

  * 74KIAS

  * Flaps up

  * Landing light off

* Lookout, 500ft begin climbing turn onto crosswind

* Use heading bug

* Use reference point

* Level off @ 1000ft

##### Levelling off

* Attitude

* Power

* Trim

##### Turning onto downwind

* Radio call immediately

  * Callsign

  * Downwind

  * Touch and go (or Full stop)

### Downwind Pre-landing Checks

* **B**rakes

* **U**nder-carriage

* **M**ixture full rich

* **F**uel quantity

* **I**nstruments (Pressures and Temperatures)

* **S**witches e.g. landing light ON

* **H**atches and harnesses

### Abeam runway threshold

* Reduce power to 2000rpm

* Flap 10 degrees (< 110KIAS)

* Trim

* Turn base at ~85KIAS, 45 degrees from runway threshold

### Base turn

* Roll

* Power, reduce to ~1500rpm

* Flap at 20 degrees (< 85KIAS)

* Trim

* On base, half sky-half ground attitude

* Should give ~75KIAS

* Half-way around, half-way down (750ft when half along base)

### Turning final

* Approach Work Cycle

  * Aimpoint - aim at runway numbers, use attitude

  * Aspect - how the runway appears

  * Airspeed - 70KIAS for 20 degree flap, 65KIAS for 30 degree flap

* Flare

  * At ~50ft AGL

  * Idle power

  * Runway threshold markers about to disappear

  * Attitude from nose down to straight and level

  * Look away from runway numbers to far end of runway

* Hold off

  * horizon at dash cowling

  * let the aircraft land

----

### Work Cycle

* **H**eight

* **S**peed

* **H**eading

* **S**pacing
