# Questions

### Straight and Level *(20151214)*

* **What is the work flow for changing cruise?**

  Power, Attitude, Speed, Trim (PAST)

* **What is the work flow for maintaining cruise?**

  Attitude, Lookout, Attitude, Performance (ALAP)

* **What are the four forces at straight and level?**

  Lift, Weight, Thrust, Drag

* **What is the effect of the four forces at straight and level?**

  They all equal zero.

* **Under straight and level, AoA increases. What other effect occurs?**

  IAS decreases

* **Under straight and level, IAS increases. What other effect occurs?**

  AoA decreases

* **Under straight and level, AoA decreases. What other effect occurs?**

  IAS increases

* **Under straight and level, IAS decreases. What other effect occurs?**

  AoA increases

* **What are the two types of drag?**

  Induced, Parasitic

* **What are the three types of parasite drag?**

  Form, Skin Friction, Interference

* **Which of the two types of drag is most pronounced?**

  Induced drag

* **What are the effects of induced drag and parasite drag?**

  Induced drag causes nose-down attitude. Parasite drag causes nose-up attitude
  to a lesser effect. The horizontal stabiliser causes a nose-up pitch to
  account for the difference.

* **What are the three types of stability?**

  Positive, Neutral, Negative

* **What is the type and axis of stability of the longitudinal axis of roll?**

  Positive stability on pitch

* **What is the type and axis of stability of the lateral axis of pitch?**

  Neutral stability on roll

* **What is the axis of stability of the directional axis?**

  Yaw

* **What is the application of normal cruise?**

  Power at 2300rpm, "5 fingers" attitude, gives performance of 100KIAS

* **What is the application of fast cruise?**

  Power at 2500rpm, "6 fingers" attitude, gives performance of 110KIAS

* **What is the application of slow cruise?**

  Power at 2100rpm, "4 fingers" attitude, gives performance of 80KIAS

* **What is the application of slow cruise with flaps?**

  Power at 2100rpm, "5 fingers" attitude, gives performance of 70KIAS

* **What is the lift equation?**

  lift equals the coefficient of lift multiplied 0.5 rho multiplied velocity
  squared mutiplied wing area.

  lift equals lift coefficient multipled 0.5 multipled air density multipled
  velocity squared multipled wing area.

  `L = Cl * .5 * r * V^2 * A`

----

### Climbing and Descending *(20151218)*

##### Climbing

* **What is VCW?**

  The Vertical Component of Weight.

* **What is RCW?**

  The Rearward Component of Weight.

* **What is the definition of a climb?**

  An increase in altitude at constant airspeed.

* **In a climb, what components make up thrust?**

  Drag + RCW.

* **What is Vx?**

  Best angle of climb.

* **How is Vx calculated?**

  Vx is the greatest difference between Thrust Available (TA) and Thrust
  Required (TR) on a thrust versus speed graph.

* **What is Vy?**

  Best rate of climb.

* **How is Vy calculated?**

  Vy is the greatest difference between Power Available (PA) and Power Required
  (PR) on a power versus speed graph.

* **If weight is decreased, what happens to best angle of climb?**

  increase
 
* **If weight is decreased, what happens to best rate of climb?**

  increase

* **If flaps are extended, what happens to best angle of climb?**

  decrease
 
* **If flaps are extended, what happens to best rate of climb?**

  decrease
 
* **If head wind, what happens to best angle of climb?**

  increase
 
* **If head wind, what happens to best rate of climb?**

  unaffected

* **If altitude increases, what happens to best angle of climb?**

  decrease
 
* **If altitude increases, what happens to best rate of climb?**

  decrease
 
* **What is the power setting to achieve best angle of climb?**

  Full throttle

* **What is the power setting to achieve best rate of climb?**

  Full throttle

* **What is the power setting to achieve cruise climb?**

  Full throttle

* **What is the attitude setting to achieve best angle of climb?**

  "high", screen at horizon"

* **What is the attitude setting to achieve best rate of climb?**

  "medium", lower than Vx"

* **What is the attitude setting to achieve cruise climb?**

  "low", lower than Vy"

* **What is the performance setting to achieve best angle of climb?**

  57KIAS

* **What is the performance setting to achieve best rate of climb?**

  62KIAS

* **What is the performance setting to achieve cruise climb?**

  75-85KIAS

* **What is the rudder input to achieve best angle of climb?**

  strong

* **What is the rudder input to achieve best rate of climb?**

  medium (< Vx)

* **What is the rudder input to achieve cruise climb?**

  low (< Vy)

* **What is the work flow for a climb?**

  * Entry: Power, Attitude Speed, Trim

  * Maintain: Attitude, Lookout, Attitude, Performance

  * Exit: Attitude, Speed, Power, Trim

* **What airmanship components are considered for climbing?**

  * 500ft check lookout.

  * maintain balance with rudder using ball indicator.

  * non-abrupt power changes.

* **What threats and management from the aircraft exist during a climb?**

  Nose-up attitude obstructs view, managed with 500ft check.

* **What threats and management from the pilot exist during a climb?**

  Illness e.g. sinus infection, managed with No Fly.

* **What threats and management from the environment exist during a climb?**

  Climb into Sun obstructs view, managed by not climbing into the Sun.

##### Descending

* **What is FCW?**

  The Forward Component of Weight.

* **If weight is decreased, what happens to best angle of descent?**

  unaffected.

* **If weight is decreased, what happens to best rate of descent?**

  decrease.

* **If flaps are extended, what happens to best angle of descent?**

  increase.

* **If flaps are extended, what happens to best rate of descent?**

  increase.

* **If head wind, what happens to best angle of descent?**

  increase.

* **If head wind, what happens to best rate of descent?**

  neutral.

* **What is the power setting for a glide descent?**

  Idle.

* **What is the attitude for a glide descent?**

  Straight & level.

* **What is the performance setting for a glide descent?**

  70KIAS.

* **What is the power setting for a cruise descent?**

  2000rpm.

* **What is the attitude for a cruise descent?**

  1 degree down from straight & level.

* **What is the performance setting for a cruise descent?**

  90KIAS, 500FPM.

* **What is the power setting for an approach descent?**

  1500-1700rpm.

* **What is the attitude for an approach descent?**

  "half sky/half ground" view.

* **What is the performance setting for an approach descent?**

  75KIAS, 500FPM.

* **What is the work flow for a descent?**

  Begin: Power, Attitude, Trim

  Maintain: Attitude, Lookout, Attitude, Performance

  Exit: Power, Attitude, Trim

* **What airmanship components are considered for descending?**

  * Airspace; maximum 3500ft due to YBBN traffic.

  * Airspace; the YBAF steps for approach.

  * Carburetor heat

* **What threats and management from the aircraft exist during a descent?**

  Spark plug fouling, managed by warming the engine.

* **What threats and management from the pilot exist during a descent?**

  Illness, managed with No Fly.

* **What threats and management from the environment exist during a descent?**

  Clouds, managed with lookout and no descent into clouds.

##### Climbing

* **Climb the aircraft at best angle**

  * Begin climb

    * Rudder high

    * **P**ower at full throttle

    * **A**ttitude top of screen to horizon

    * **S**peed at 57KIAS

    * **T**rim

  * Maintain climb

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance

  * Exit climb

    * **A**ttitude

    * **S**peed

    * **P**ower

    * **T**rim

* **Climb the aircraft at best rate**

  * Begin climb

    * Rudder medium

    * **P**ower at full throttle

    * **A**ttitude dashboard coaming to horizon

    * **S**peed at 62KIAS

    * **T**rim

  * Maintain climb

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance

  * Exit climb

    * **A**ttitude

    * **S**peed

    * **P**ower

    * **T**rim

* **Climb the aircraft at cruise**

  * Begin climb

    * Rudder low

    * **P**ower at full throttle

    * **A**ttitude nose to horizon

    * **S**peed at 75-85KIAS

    * **T**rim

  * Maintain climb

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance

  * Exit climb

    * **A**ttitude

    * **S**peed

    * **P**ower

    * **T**rim

##### Descending

* **Descend the aircraft at glide**

  * Begin descent

    * **P**ower to idle

    * **A**ttitude to straight and level

    * **T**rim

  * Maintain descent

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance at 70KIAS

  * Exit descent

    * **P**ower

    * **A**ttitude

    * **T**rim

* **Descend the aircraft at cruise**

  * Begin descent

    * **P**ower to 2000rpm

    * **A**ttitude to 1 degree down from straight and level

    * **T**rim

  * Maintain descent

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance at 90KIAS, 500FPM

  * Exit descent

    * **P**ower

    * **A**ttitude

    * **T**rim

* **Descend the aircraft at approach**

  * Begin descent

    * **P**ower to 1500-1700rpm

    * **A**ttitude to half sky/half ground

    * **T**rim

  * Maintain descent

    * **A**ttitude

    * **L**ookout 500ft check

    * **A**ttitude

    * **P**erformance at 75KIAS, 500FPM

  * Exit descent

    * **P**ower

    * **A**ttitude

    * **T**rim

----

### Turning *(20151220)*

* **Which force turns an aircraft?**

  The horizontal component of lift (HCL).

* **What makes up a Load Factor?**

  The ratio of lift to weight.

* **Given a constant IAS and increasing AoB, what happens to Turn Radius and Rate
  of Turn?**

  Turn Radius decreases, Rate of Turn increases
  
* **Given an increasing IAS and constant AoB, what happens to Turn Radius and Rate
  of Turn?**

  Turn Radius increases, Rate of Turn decreases

* **What is adverse aileron yaw and how is it counteracted [on the C162]?**

  The tendency to yaw out of a turn. Counteracted with a Frise Aileron and
  Differential Aileron.

* **Under a left turn, the inclinometer is left of centre. Respond.**
 
  The aircraft is slipping and requires left rudder input to balance.

* **Under a left turn, the inclinometer is right of centre. Respond.**
 
  The aircraft is skidding and requires right rudder input to balance.

* **Under a right turn, the inclinometer is right of centre. Respond.**
 
  The aircraft is slipping and requires right rudder input to balance.

* **Under a right turn, the inclinometer is left of centre. Respond.**
 
  The aircraft is skidding and requires left rudder input to balance.

* **What is overbanking?**

  A tendency to roll on a climbing turn, due to a greater AoA on the outer wing.
  Corrected with aileron.

* **What is underbanking?**

  A tendency to roll on a descending turn due to a greater AoA on the inner
  wing. Corrected with aileron.

* **What is a gentle turn?**

  Up to 15 degrees AoB.

* **What is a rate 1 turn?**

  A turn that takes two minutes, calculated by `(TAS + 7)/10`, approximately 15
  degrees AoB for C162.

* **What is a medium turn?**

  Up to 30 degrees AoB.

* **What is the limit for a climbing turn?**

  15 degrees AoB.

* **What is the limit for a descending turn?**

  30 degrees AoB.

* **Apply a turn.**

  1. Lookout

  2. Select reference

  3. Select altitude

  4. Begin turn: ALAP

  5. Bank with aileron

  6. Balance for slip & skid with rudder

  7. Backpressure on elevator

  8. End turn: ALAP

* **What airmanship principles apply to turning?**

  Clarity on who is in control. Handing over & taking over.

* **What threats apply to the aeroplane during a turn?**

  The high-wing of the C162 obscures vision, managed with lookout.

* **What threats apply to the pilot during a turn?**
 
  Disorientation, managed with handing over.
  
* **What threats exist from the environment during a turn?**
 
  Terrain, managed with lookout.

----

### Stalling *(20160104)*

* **What is the relevance of the Critical Angle of Attack?**

  The Angle of Attack (chord line to RAF) at which the lift coefficient is at
  its maximum and the aircraft stalls. Increasing AoA after Critical rapidly
  reduces the lift coefficient.

* **What is the effect of weight on an aircraft stall speed?**

  As weight increases, stall speed increases.

* **What is the effect of power on an aircraft stall speed?**

  As power increases, stall speed decreases.

* **What is the effect of flap on an aircraft stall speed?**

  Flap extension, stall speed decreases. Flap retraction, stall speed
  increases.

* **What is the effect of load factor on an aircraft stall speed?**

  As load factor increases, stall speed increases.

* **What is the HASELL process?**

  * **H**eight: Must recover by 3000AGL

  * **A**irframe: Configured (flap up)

  * **S**ecurity: No loose objects, hatches and harness secure

  * **E**ngine: Pressures and temps checked

  * **L**ocation: Not over built-up areas

  * **L**ookout: 360 degree clearing turn before commencing and 90 degree
     between each stall

* **An aircraft with Vs = [35-65]KIAS performs a [10,20,30,45,60] degree turn.
   What is the new stall speed?**

  `LF = 1 / cos AoB`
   
  `New stall speed = Vs * (LF ^ 0.5)`

* **What is a consequence of aileron in a stall?**

  Aileron on the stalled down wing will increase the AoA of the outer wing,
  resulting in wingdrop and possible spin.

* **What are the indicators and recovery of an impending stall?**

  * Slow and decaying airspeed

  * Nose-high attitude

  * Less wind noise

  * Controls less responsive

  * Stall warning (5-8KIAS above Critical AoA)

  * Control buffet

  * To recover, lower nose

* **What are the indicators and recovery of a developed stall?**

  * loss of height *(guaranteed)*

  * nose drop *(maybe)*

  * wing drop *(maybe)*

  * To recover, lower nose to level, add full power, right rudder. After
    sufficient airspeed, raise nose slightly

* **What are common faults of stall recovery?**

  * lowering nose too far (do not exceed loss of > 100ft altitude)

  * delayed application of full power

  * pull up too quickly after recovery -> secondary stall

* **What threats exist in inducing a stall?**

  Low power setting can cause carburetor icing. Apply carburetor heat until
  stall warning, then turn off.

----

# Circuit Emergencies *(20160212)*

* **What are the parameters for a go-around?**

  There are no specified limits. Set personal limits based on conditions,
  aircraft type, etc.

* **Execute a go-around**

  1. Full power

  2. Raise nose to cowl on horizon

  3. Flap up one stage, 30 to 20

  4. Achieve minimum 56KIAS

  5. At ~60KIAS, flap up one stage, 20 to 10

  6. At ~65KIAS, flap retracted, 10 to 0

  7. Raise nose to climb

* **Execute a flapless approach**

  1. Trim nose up

  2. Higher attitudes than normal

  3. Trim substitutes flap

  4. 75-80KIAS on base

  5. Power for speed

  6. 70KIAS approach

  7. Threshold on top of cowling

* **Execute Engine Failure After Take-Off prior to rotate**

  1. Close the throttle

  2. Apply maximum braking

  3. Exit the runway at nearest taxiway

* **Execute Engine Failure After Take-Off after rotate with runway**

  1. Lower the nose 

  2. Close the throttle

  3. Land the aircraft on the remaining runway

  4. Apply maximum braking

  5. Exit the runway at nearest taxiway

* **Execute Engine Failure After Take-Off after prior to rotate without runway**

  1. Lower the nose to glide

  2. Select landing area within 30 degrees of heading

  3. Commence a turn only if:

    * already commenced a turn

    * sufficient glide to runway

