# Effects of controls

* **Function of Elevator**

  * Primary function: Pitch
  * Secondary function: Airspeed
  
* **Function of Aileron**

  * Primary function: Roll
  * Secondary function: Yaw

* **Function of Rudder**

  * Primary function: Yaw
  * Secondary function: Roll

# Effect on controls

* **What is the effect on controls of airspeed?**

  * **Fast** causes firm and responsive controls.
  * **Slow** causes sloppy and less responsive controls.

* **What is the effect on controls of power?**

  * **High power** causes nose up pitch and left yaw, requiring right rudder.
  * **Low power** causes nose down pitch and right yaw, requiring left rudder.

* **What is the effect on controls of Slipstream?**

  **High power** causes the left yaw corkscrew effect and tail controls become
  more effective.

# Ancillary Controls

* **Throttle**

  Push for Power

* **Mixture (carburretor)**

  * Forward: rich

  * Backward: lean

* **Heat**

  * Hot air to carburretor to de-ice.

  * Back: on

  * Forward: off

* **Fuel tanks/selector**

  * Selects required tank, or both.

* **Trimming**

  * Most commonly trimmed: Elevator, Rudder.

* **Flaps**

  * Increase forward visibility (change in camber).

  * Increase lift production.

  * Use with airspeed in white arc (airspeed indicator).
